package com.example.first_project.cinema.repository;

import com.example.first_project.cinema.domain.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

    @Query(
            "select case when count(KEY(tickets))> 0 then true else false end " +
                    "from Cart c join c.ticketsCountMap tickets " +
                    "where c =:cart and KEY(tickets).id = :ticketId"
    )
    boolean cartContainsTicket(
            @Param("cart") Cart cart,
            @Param("ticketId") Long ticket
    );
}
