package com.example.first_project.cinema.repository;

import com.example.first_project.cinema.domain.Movie;
import com.example.first_project.cinema.domain.MovieTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieTimeRepository extends JpaRepository<MovieTime, Long> {

    List<MovieTime> getAllByMovie(Movie movie);

    boolean existsByMovieId(Long id);
}
