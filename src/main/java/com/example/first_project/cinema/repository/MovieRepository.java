package com.example.first_project.cinema.repository;

import com.example.first_project.cinema.domain.Category;
import com.example.first_project.cinema.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    @Query(value = "select m.* from _movie m " +
            "inner join category_movie cm on cm.movie_id = m.id" +
            " where cm.category_id =:id", nativeQuery = true)

   List<Movie> findAllByCategory(@Param("id") Long id);

    List<Movie> findAllByCategories(Category category);
}
