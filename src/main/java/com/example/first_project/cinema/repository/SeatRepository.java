package com.example.first_project.cinema.repository;

import com.example.first_project.cinema.domain.Movie;
import com.example.first_project.cinema.domain.Seat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SeatRepository extends JpaRepository<Seat, Long> {

    List<Seat> getAllByMovie(Movie movie);

//    Page<Seat> getAllByShopId(Long id, Pageable pageable);

    boolean existsByMovieId(Long id);
}
