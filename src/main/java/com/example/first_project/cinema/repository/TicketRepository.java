package com.example.first_project.cinema.repository;

import com.example.first_project.cinema.domain.Movie;
import com.example.first_project.cinema.domain.Seat;
import com.example.first_project.cinema.domain.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    Ticket getBySeats(Seat seat);

    boolean existsBySeatsId(Long id);
}
