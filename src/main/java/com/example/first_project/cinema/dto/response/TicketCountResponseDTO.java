package com.example.first_project.cinema.dto.response;

import com.example.first_project.cinema.domain.Seat;
import com.example.first_project.cinema.domain.Ticket;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TicketCountResponseDTO {

    private TicketResponseDTO ticket;

    private Double count;

    public TicketCountResponseDTO(Ticket ticket, Double count) {
        this.ticket = new TicketResponseDTO(ticket);
        this.count = count;
    }
}
