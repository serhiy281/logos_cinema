package com.example.first_project.cinema.dto.response;

import com.example.first_project.cinema.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter

public class UserResponseDTO {

    private String username;

    public UserResponseDTO(User user) {
        this.username = user.getUsername();
    }
}
