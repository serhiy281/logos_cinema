package com.example.first_project.cinema.dto.request;

import lombok.Getter;

@Getter
public class CategoryRequestDTO {
    private String name;
}
