package com.example.first_project.cinema.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieTimeRequestDTO {

    private String time;

    private Long movieId;
}
