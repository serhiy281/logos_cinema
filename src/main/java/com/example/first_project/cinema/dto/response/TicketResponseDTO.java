package com.example.first_project.cinema.dto.response;

import com.example.first_project.cinema.domain.Ticket;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TicketResponseDTO {

    private Long id;
    private Double price;

    public TicketResponseDTO(Ticket ticket) {
        this.id = ticket.getId();
        this.price = ticket.getPrice();
    }
}
