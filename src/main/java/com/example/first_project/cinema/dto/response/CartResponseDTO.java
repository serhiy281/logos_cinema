package com.example.first_project.cinema.dto.response;

import com.example.first_project.cinema.domain.Cart;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class CartResponseDTO {

    private Double sum = 0.0;

    private List<TicketCountResponseDTO> tickets = new ArrayList<>();

    public CartResponseDTO(Cart cart) {
        tickets = cart.getTicketsCountMap()
                .entrySet()
                .stream()
                .peek(e -> sum += e.getKey().getPrice() * e.getValue())
                .map(e -> new TicketCountResponseDTO(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }
}
