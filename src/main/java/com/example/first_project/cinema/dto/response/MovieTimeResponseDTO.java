package com.example.first_project.cinema.dto.response;

import com.example.first_project.cinema.domain.MovieTime;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieTimeResponseDTO {

    private String time;

    public MovieTimeResponseDTO(MovieTime movieTime) {
        this.time = movieTime.getTime();
    }
}
