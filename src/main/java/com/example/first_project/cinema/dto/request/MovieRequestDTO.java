package com.example.first_project.cinema.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
public class MovieRequestDTO {

    private String name;

    private String image;

    private Set<Long> categoryIds = new HashSet<>();
}
