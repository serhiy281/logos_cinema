package com.example.first_project.cinema.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
public class SeatRequestDTO {



    private Long ticketId;
    private Long movieId;
    private Long movieTimeId;


}
