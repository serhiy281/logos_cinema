package com.example.first_project.cinema.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthenticationResponseDTO {

    private String username;
    private String token;
    private Long id;




    public AuthenticationResponseDTO(String username, String token, Long id) {
        this.username = username;
        this.token = token;
        this.id = id;
    }
}
