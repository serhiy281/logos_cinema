package com.example.first_project.cinema.dto.response;

import com.example.first_project.cinema.domain.Movie;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class MovieResponseDTO {

    private String name;
    private Long id;
    private List<CategoryResponseDTO> categories = new ArrayList<>();
    private String image;

    public MovieResponseDTO(Movie movie) {
        this.name = movie.getName();
        this.id = movie.getId();
        this.image = movie.getImage();
        if (movie.getCategories() != null) {
            this.categories = movie.getCategories()
                    .stream()
                    .map(CategoryResponseDTO::new)
                    .collect(Collectors.toList());
        }
    }
}
