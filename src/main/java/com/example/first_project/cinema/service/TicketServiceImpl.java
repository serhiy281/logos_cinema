package com.example.first_project.cinema.service;

import com.example.first_project.cinema.domain.Seat;
import com.example.first_project.cinema.domain.Ticket;
import com.example.first_project.cinema.dto.request.SeatRequestDTO;
import com.example.first_project.cinema.dto.request.TicketRequestDTO;
import com.example.first_project.cinema.repository.SeatRepository;
import com.example.first_project.cinema.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private SeatService seatService;

    @Override
    public void save(TicketRequestDTO ticket) throws IOException {
        ticketRepository.save(mapTicketRequestToTicket(ticket, null));
    }

    @Override
    public Ticket getById(Long id) {
        return ticketRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Ticket with id " + id + " doesnt exist"));
    }

    @Override
    public List<Ticket> getAll() {
        return ticketRepository.findAll();
    }

    @Override
    public Ticket update(TicketRequestDTO ticket, Long id) throws IOException {
        return ticketRepository.save(mapTicketRequestToTicket(ticket, getById(id)));
    }

    @Override
    public void delete(Long id) {
        ticketRepository.deleteById(id);
    }

    private Ticket mapTicketRequestToTicket(TicketRequestDTO ticketRequestDTO, Ticket ticket) throws IOException {
        if (ticket == null) {
            ticket = new Ticket();
        }
        ticket.setPrice(ticketRequestDTO.getPrice());

        return ticket;
    }
}
