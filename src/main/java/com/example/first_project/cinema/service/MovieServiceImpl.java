package com.example.first_project.cinema.service;

import com.example.first_project.cinema.domain.Category;
import com.example.first_project.cinema.domain.Movie;
import com.example.first_project.cinema.dto.request.MovieRequestDTO;
import com.example.first_project.cinema.repository.MovieRepository;
import com.example.first_project.cinema.tools.FileTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private FileTool fileTool;

    @Override
    public Movie save(MovieRequestDTO movie) throws IOException {
        Movie savedMovie = movieRepository.save(mapMovieRequestDTOToMovie(movie, null));
        return movieRepository.save(addCategoriesToMovie(savedMovie, movie));
    }

    @Override
    public Movie update(MovieRequestDTO movieRequestDTO, Long id) throws IOException{
        Movie movie = mapMovieRequestDTOToMovie(movieRequestDTO, getById(id));
        return movieRepository.save(addCategoriesToMovie(movie, movieRequestDTO));
    }

    @Override
    public Movie getById(Long id) {
        return movieRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
    }

    @Override
    public List<Movie> getAll() {
        return movieRepository.findAll();
    }

    @Override
    public List<Movie> getAllByCategory(Long categoryId) {
        return movieRepository.findAllByCategories(categoryService.getById(categoryId));
    }




    @Override
    public void delete(Long id) {
        movieRepository.deleteById(id);
    }

    private Movie addCategoriesToMovie(Movie movie, MovieRequestDTO movieRequestDTO) {
        if (movieRequestDTO.getCategoryIds() != null) {
            if (movie.getCategories() != null) {
                for (Category category : movie.getCategories()) {
                    category.getMovies().remove(movie);
                }
            }
                movieRequestDTO.getCategoryIds().forEach((id) -> {
                    Category category = categoryService.getById(id);
                    movie.getCategories().add(category);
                    category.getMovies().add(movie);
                });
        }
        return movie;
    }

    private Movie mapMovieRequestDTOToMovie(MovieRequestDTO movieRequestDTO, Movie movie) throws IOException {
        if (movie == null) {
            movie = new Movie();
        }
        movie.setName(movieRequestDTO.getName());
        if (movieRequestDTO.getImage() != null) {
            movie.setImage(fileTool.saveFile(movieRequestDTO.getImage()));
        }
        return movie;
    }
}
