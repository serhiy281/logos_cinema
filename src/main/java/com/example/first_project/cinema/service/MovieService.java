package com.example.first_project.cinema.service;

import com.example.first_project.cinema.domain.Movie;
import com.example.first_project.cinema.dto.request.MovieRequestDTO;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;


public interface MovieService {
     Movie save(MovieRequestDTO movie) throws IOException;

    Movie update(MovieRequestDTO movie, Long id) throws IOException;

    Movie getById(Long id);

    List<Movie> getAll();

    List<Movie> getAllByCategory(Long categoryId);



    void delete(Long id);
}
