package com.example.first_project.cinema.service;

import com.example.first_project.cinema.domain.Seat;
import com.example.first_project.cinema.dto.request.SeatRequestDTO;

import java.io.IOException;
import java.util.List;

public interface SeatService {

    void save(SeatRequestDTO seat) throws IOException;

     Seat getById(Long id);


    List<Seat> getAll();

//    Page<Seat> getPageByShopId(@NotNull SeatSearchRequestDTO searchRequest);

    Seat update(SeatRequestDTO seat, Long id) throws IOException;

    void delete(Long id);
}
