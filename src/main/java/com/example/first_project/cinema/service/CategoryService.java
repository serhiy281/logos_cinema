package com.example.first_project.cinema.service;

import com.example.first_project.cinema.domain.Category;
import com.example.first_project.cinema.dto.request.CategoryRequestDTO;

import java.util.List;

public interface CategoryService {

    void create(CategoryRequestDTO requestDTO);

    Category getById(Long id);

    List<Category> getAll();
}
