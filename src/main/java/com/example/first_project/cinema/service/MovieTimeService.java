package com.example.first_project.cinema.service;


import com.example.first_project.cinema.domain.Movie;
import com.example.first_project.cinema.domain.MovieTime;
import com.example.first_project.cinema.dto.request.MovieRequestDTO;
import com.example.first_project.cinema.dto.request.MovieTimeRequestDTO;

import java.io.IOException;
import java.util.List;

public interface MovieTimeService {

    void save(MovieTimeRequestDTO movie) throws IOException;

    MovieTime update(MovieTimeRequestDTO movie, Long id) throws IOException;

    MovieTime getById(Long id);

    List<MovieTime> getAll();

    void delete(Long id);
}
