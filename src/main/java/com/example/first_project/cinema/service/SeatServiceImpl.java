package com.example.first_project.cinema.service;

import com.example.first_project.cinema.domain.Seat;
import com.example.first_project.cinema.dto.request.SeatRequestDTO;
import com.example.first_project.cinema.dto.request.TicketRequestDTO;
import com.example.first_project.cinema.repository.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class SeatServiceImpl implements SeatService {

    @Autowired
    private SeatRepository seatRepository;

    @Autowired
    private MovieService movieService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private MovieTimeService movieTimeService;


    @Override
    public void save(SeatRequestDTO seat) throws IOException {
        seatRepository.save(mapSeatRequestToSeat(seat, null));
    }

    @Override
    public Seat getById(Long id) {
        return seatRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Seat with id " + id + " doesnt exist"));
    }

    @Override
    public List<Seat> getAll() {
        return seatRepository.findAll();
    }

//    @Override
//    public Page<Seat> getPageByShopId(SeatSearchRequestDTO searchRequest) {
//        return seatRepository.findAll(
//                new SeatSpecification(searchRequest),
//                searchRequest.getPagination().mapToPageable()
//        );
//    }


    @Override
    public Seat update(SeatRequestDTO seat, Long id) throws IOException {
        return seatRepository.save(mapSeatRequestToSeat(seat, getById(id)));
    }

    @Override
    public void delete(Long id) {
        seatRepository.deleteById(id);
    }

    private Seat mapSeatRequestToSeat(SeatRequestDTO seatRequestDTO, Seat seat) throws IOException {
        if (seat == null) {
            seat = new Seat();
        }
        seat.setMovie(movieService.getById(seatRequestDTO.getMovieId()));
        seat.setTicket(ticketService.getById(seatRequestDTO.getTicketId()));
        seat.setMovieTimes(movieTimeService.getById(seatRequestDTO.getMovieTimeId()));
        return seat;
    }
}
