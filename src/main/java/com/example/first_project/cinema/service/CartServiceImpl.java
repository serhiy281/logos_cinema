package com.example.first_project.cinema.service;

import com.example.first_project.cinema.domain.Cart;

import com.example.first_project.cinema.domain.Ticket;
import com.example.first_project.cinema.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartServiceImpl implements CartService {

    private CartRepository cartRepository;

    private TicketService ticketService;

    private UserService userService;

    @Autowired
    public CartServiceImpl(CartRepository cartRepository, TicketService ticketService, UserService userService) {
        this.cartRepository = cartRepository;
        this.ticketService = ticketService;
        this.userService = userService;
    }




    @Override
    public void addTicket(Long cartId, Long ticketId) {
        Cart cart = getById(cartId);

        Ticket ticket = ticketService.getById(ticketId);
        cart.getTicketsCountMap().putIfAbsent(ticket, 1.0);

        cartRepository.save(cart);
    }



    @Override
    public Cart getById(Long id) {
        return cartRepository.getById(id);
    }

    @Override
    public Cart getByCurrentUser() {
        return userService.getCurrentUser().getCart();
    }
}
