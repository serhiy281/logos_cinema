package com.example.first_project.cinema.service;

import com.example.first_project.cinema.domain.User;
import com.example.first_project.cinema.dto.request.UserLoginRequestDTO;
import com.example.first_project.cinema.dto.request.UserRegistrationRequestDTO;
import com.example.first_project.cinema.dto.response.AuthenticationResponseDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    AuthenticationResponseDTO registerUser(UserRegistrationRequestDTO requestDTO);
    AuthenticationResponseDTO login(UserLoginRequestDTO requestDTO);

    User findByLogin(String username);
    User getCurrentUser();

}
