package com.example.first_project.cinema.service;


import com.example.first_project.cinema.domain.Ticket;

import com.example.first_project.cinema.dto.request.TicketRequestDTO;

import java.io.IOException;
import java.util.List;

public interface TicketService {

    void save(TicketRequestDTO ticket) throws IOException;

    Ticket getById(Long id);

    List<Ticket> getAll();

    Ticket update(TicketRequestDTO ticket, Long id) throws IOException;

    void delete(Long id);
}
