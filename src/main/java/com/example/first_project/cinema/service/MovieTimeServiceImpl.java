package com.example.first_project.cinema.service;

import com.example.first_project.cinema.domain.MovieTime;
import com.example.first_project.cinema.dto.request.MovieTimeRequestDTO;
import com.example.first_project.cinema.repository.MovieTimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class MovieTimeServiceImpl implements MovieTimeService {

    @Autowired
    private MovieTimeRepository movieTimeRepository;

    @Autowired
    private MovieService movieService;


    @Override
    public void save(MovieTimeRequestDTO movie) throws IOException {
        movieTimeRepository.save(mapMovieTimeRequestDTOToMovieTime(movie, null));
    }


    @Override
    public MovieTime update(MovieTimeRequestDTO movie, Long id) throws IOException {
        return movieTimeRepository.save(mapMovieTimeRequestDTOToMovieTime(movie, getById(id)));
    }

    @Override
    public MovieTime getById(Long id) {
        return movieTimeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("MovieTime with id " + id + " doesnt exist"));
    }

    @Override
    public List<MovieTime> getAll() {
        return movieTimeRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        movieTimeRepository.deleteById(id);
    }

    private MovieTime mapMovieTimeRequestDTOToMovieTime(MovieTimeRequestDTO movieTimeRequestDTO, MovieTime movieTime) throws IOException {
        if (movieTime == null) {
            movieTime = new MovieTime();
        }
        movieTime.setMovie(movieService.getById(movieTimeRequestDTO.getMovieId()));
        return movieTime;
    }
}
