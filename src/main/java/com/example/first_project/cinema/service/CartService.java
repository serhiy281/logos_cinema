package com.example.first_project.cinema.service;

import com.example.first_project.cinema.domain.Cart;

public interface CartService {

    void addTicket(Long cartId, Long ticketId);

    Cart getById(Long id);

    Cart getByCurrentUser();
}
