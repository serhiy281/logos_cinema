package com.example.first_project.cinema.controller;

import com.example.first_project.cinema.dto.request.MovieTimeRequestDTO;
import com.example.first_project.cinema.dto.request.SeatRequestDTO;
import com.example.first_project.cinema.dto.response.MovieTimeResponseDTO;
import com.example.first_project.cinema.dto.response.SeatResponseDTO;
import com.example.first_project.cinema.service.MovieTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/movie-times")
public class MovieTimeController {

    @Autowired
    private MovieTimeService movieTimeService;

    @PostMapping
    private void createMovieTime(@RequestBody MovieTimeRequestDTO movieTime) throws IOException {
        movieTimeService.save(movieTime);
    }

    @PutMapping("/{id}")
    private MovieTimeResponseDTO updateMovieTime(@RequestBody MovieTimeRequestDTO movieTime, @PathVariable("id") Long id) throws IOException {
        return new MovieTimeResponseDTO(movieTimeService.update(movieTime, id));
    }

    @GetMapping("/{id}")
    private MovieTimeResponseDTO getById(@PathVariable("id") Long id) {
        return new MovieTimeResponseDTO(movieTimeService.getById(id));
    }

    @GetMapping()
    private List<MovieTimeResponseDTO> getAll() {
        return movieTimeService.getAll()
                .stream()
                .map(MovieTimeResponseDTO::new)
                .collect(Collectors.toList());
    }



    @DeleteMapping("/{id}")
    private void deleteMovieTime(@PathVariable("id") Long id) {
        movieTimeService.delete(id);
    }
}
