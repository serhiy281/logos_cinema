package com.example.first_project.cinema.controller;
import com.example.first_project.cinema.dto.request.SeatRequestDTO;
import com.example.first_project.cinema.dto.response.SeatResponseDTO;
import com.example.first_project.cinema.service.SeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/seats")
public class SeatController {

   @Autowired
    private SeatService seatService;

    @PostMapping
    private void createSeat(@RequestBody SeatRequestDTO seat) throws IOException {
        seatService.save(seat);
    }

    @PutMapping("/{id}")
    private SeatResponseDTO updateItem(@RequestBody SeatRequestDTO seat, @PathVariable("id") Long id) throws IOException {
        return new SeatResponseDTO(seatService.update(seat, id));
    }

    @GetMapping("/{id}")
    private SeatResponseDTO getById(@PathVariable("id") Long id) {
        return new SeatResponseDTO(seatService.getById(id));
    }

    @GetMapping()
    private List<SeatResponseDTO> getAll() {
        return seatService.getAll()
                .stream()
                .map(SeatResponseDTO::new)
                .collect(Collectors.toList());
    }



    @DeleteMapping("/{id}")
    private void deleteItem(@PathVariable("id") Long id) {
        seatService.delete(id);
    }
}
