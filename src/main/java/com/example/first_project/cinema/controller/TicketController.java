package com.example.first_project.cinema.controller;

import com.example.first_project.cinema.dto.request.TicketRequestDTO;
import com.example.first_project.cinema.dto.response.TicketResponseDTO;
import com.example.first_project.cinema.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/tickets")
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @PostMapping
    private void createTicket(@RequestBody TicketRequestDTO ticket) throws IOException {
        ticketService.save(ticket);
    }

    @PutMapping("/{id}")
    private TicketResponseDTO updateItem(@RequestBody TicketRequestDTO ticket, @PathVariable("id") Long id) throws IOException {
        return new TicketResponseDTO(ticketService.update(ticket, id));
    }

    @GetMapping("/{id}")
    private TicketResponseDTO getById(@PathVariable("id") Long id) {
        return new TicketResponseDTO(ticketService.getById(id));
    }

    @GetMapping()
    private List<TicketResponseDTO> getAll() {
        return ticketService.getAll()
                .stream()
                .map(TicketResponseDTO::new)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{id}")
    private void deleteTicket(@PathVariable("id") Long id) {
        ticketService.delete(id);
    }
}
