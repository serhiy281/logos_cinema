package com.example.first_project.cinema.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@CrossOrigin
public class HTMLPagesController {

    // localhost:8080/admin/movie
    @RequestMapping(value = "/admin/movie")
    private String getAdminMoviePage() {
        return "/html/admin-movie.html";
    }

    @RequestMapping("/admin/movie-manage/{movieId}")
    private String getAdminMovieManagePage() {
        return "/html/admin-movie-manage.html";
    }

    @RequestMapping("/registration")
    private String getUserRegistrationPage() {
        return "/html/user-registration.html";
    }

    @RequestMapping("/login")
    private String getLoginPage() {
        return "/html/login.html";
    }

    @RequestMapping
    private String getMainPage() {
        return "/html/index.html";
    }
}
