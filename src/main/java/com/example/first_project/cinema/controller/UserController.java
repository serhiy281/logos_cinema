package com.example.first_project.cinema.controller;

import com.example.first_project.cinema.dto.request.UserLoginRequestDTO;
import com.example.first_project.cinema.dto.request.UserRegistrationRequestDTO;
import com.example.first_project.cinema.dto.response.AuthenticationResponseDTO;
import com.example.first_project.cinema.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {

    private UserServiceImpl userServiceImpl;


    @Autowired
    public UserController(UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;

    }

    @PostMapping("/register")
    public AuthenticationResponseDTO registerUser(@RequestBody UserRegistrationRequestDTO requestDTO) {
        return userServiceImpl.registerUser(requestDTO);
    }

    @PostMapping("/login")
    public AuthenticationResponseDTO login(@RequestBody UserLoginRequestDTO requestDTO) {
        return userServiceImpl.login(requestDTO);
    }


}

