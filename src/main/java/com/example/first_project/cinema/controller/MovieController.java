package com.example.first_project.cinema.controller;

import com.example.first_project.cinema.dto.request.MovieRequestDTO;
import com.example.first_project.cinema.dto.response.MovieResponseDTO;
import com.example.first_project.cinema.service.MovieService;
import com.example.first_project.cinema.tools.FileTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

//    @Autowired
//    private FileTool fileTool;
//
//    @PostMapping("/image")
//	private String saveImage(@RequestBody String image) throws IOException {
//		return fileTool.saveFile(image);
//	}

    @PostMapping
    private MovieResponseDTO create(@RequestBody MovieRequestDTO movie) throws IOException {
        return new MovieResponseDTO(movieService.save(movie));
    }

    @PutMapping("/{id}")
    private MovieResponseDTO update(@RequestBody MovieRequestDTO movie, @PathVariable("id") Long id) throws IOException {
        return new MovieResponseDTO(movieService.update(movie, id));
    }

    @GetMapping("/{id}")
    private MovieResponseDTO get(@PathVariable("id") Long id) {
        return new MovieResponseDTO(movieService.getById(id));
    }

    @DeleteMapping("/{id}")
    private void delete(@PathVariable("id") Long id) {
        movieService.delete(id);
    }

    @GetMapping()
    private List<MovieResponseDTO> getAll() {
        return movieService.getAll()
                .stream()
                .map(MovieResponseDTO::new)
                .collect(Collectors.toList());
    }

    @GetMapping("/category/{categoryId}")
    private List<MovieResponseDTO> getAllByCategory(@PathVariable("categoryId") Long id) {
        return movieService.getAllByCategory(id)
                .stream()
                .map(MovieResponseDTO::new)
                .collect(Collectors.toList());
    }

}
