package com.example.first_project.cinema.controller;

import com.example.first_project.cinema.dto.response.CartResponseDTO;
import com.example.first_project.cinema.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/carts")
public class CartController {

    private CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @PutMapping("/add-ticket")
    private void addTicketToCart(Long id, Long ticketId) {
        cartService.addTicket(id, ticketId);
    }

    @GetMapping
    private CartResponseDTO getCart() {
        return new CartResponseDTO(cartService.getByCurrentUser());
    }
}
