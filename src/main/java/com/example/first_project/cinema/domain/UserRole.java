package com.example.first_project.cinema.domain;

public enum UserRole {
    ROLE_USER, ROLE_ADMIN
}
