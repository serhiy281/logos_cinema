package com.example.first_project.cinema.domain;

public enum EntityStatus {
    ACTIVE, DELETED
}
