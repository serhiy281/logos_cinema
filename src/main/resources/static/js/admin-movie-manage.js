// import {httpGet, httpPost, httpPut} from './http-tools.js'
// import {appendHeader} from './header.js'
// import {Modal} from './modal.js'
//
// document.addEventListener('DOMContentLoaded', function () {
//     appendHeader().then(r => {
//
//
//          nextPageButtonOnClick();
//          prevPageButtonOnClick();
//           // sortingSelectOnChange();
//          createSeatButtonOnClick();
//          // actionsOnFileLoad();
//          // actionsOnAddImageClick();
//          openModal();
//         loadSeats();
//         loadMovie();
//     });
//
// });
//
// let movie = {
//     id: -1,
//     name: '',
//     categories: []
// };
// // let image = null;
//
// const pagination = {
//     page: 0,
//     size: 10,
//     totalPages: 0,
//     totalElements: 0
// }
//
// const loadMovie = () => {
//     // const params = new URLSearchParams(window.location.search);
//     let request = httpGet("http://localhost:8080/movies/" + getMovieId());
//
//     if (request.status === 200) {
//         movie = JSON.parse(request.response);
//         console.log(movie);
//         appendMovieData();
//     }
// }
//
// // const actionsOnAddImageClick = () => {
// //     let button = document.getElementById('add-image-button');
// //     let fileInput = document.getElementById('movie-image-input');
// //     button.onclick = () => {
// //         fileInput.click()
// //     }
// // }
//
// // const actionsOnFileLoad = () => {
// //     let fileInput = document.getElementById('movie-image-input');
// //     fileInput.onchange = (event) => {
// //         console.log(event.target.files[0]);
// //         getBase64FromFile(event.target.files[0])
// //             .then(data => {
// //                 image = data;
// //                 let imagePreview = document.getElementById('image-preview');
// //                 imagePreview.setAttribute('src', image);
// //                 imagePreview.hidden = false;
// //             });
// //     }
// // }
//
// // const getBase64FromFile = (file) => {
// //     return new Promise((resolve, reject) => {
// //         const reader = new FileReader();
// //         reader.readAsDataURL(file);
// //         reader.onload = () => resolve(reader.result);
// //         reader.onerror = error => reject(error);
// //     });
// // };
//
// const appendMovieData = () => {
//     const movieNameLabel = document.getElementById('movie-name-label');
//     movieNameLabel.innerText = movie.name;
//
//     const movieCategoriesContainer = document.getElementById('movie-categories-container');
//     movieCategoriesContainer.innerHTML = '';
//
//     if (movie.categories && movie.categories.length > 0) {
//         for (let category of movie.categories) {
//             movieCategoriesContainer.innerHTML = movieCategoriesContainer.innerHTML + getCategoryWrapper(category)
//         }
//     } else {
//         movieCategoriesContainer.innerHTML = `<span class="hor-space id-text">no categories</span>`;
//     }
// }
//
// const getCategoryWrapper = (category) => {
//     return `<span class="category-wrapped">${category.name}</span>`
// }
//
// const getMovieId = () => {
//     const pathnameSections = window.location.pathname.split('/');
//     return pathnameSections[pathnameSections.length - 1];
// }
//
// const createSeatButtonOnClick = () => {
//     let createButton = document.getElementById('create-seat-button');
//
//     createButton.onclick = () => {
//         // let countInput = document.getElementById('seat-count-input');
//
//         const seat = {
//
//             movieId: movie.id
//
//         }
//
//         console.log(seat);
//         let request = httpPost('http://localhost:8080/seats', JSON.stringify(seat));
//         if (request.status === 200) {
//             // window.location.reload();
//             loadSeats();
//         }
//     }
// }
//
// const nextPageButtonOnClick = () => {
//     let nextPageBtn = document.getElementById('next-page-button');
//     nextPageBtn.onclick = () => {
//         pagination.page = pagination.page + 1;
//         loadSeats();
//     }
// }
//
// const prevPageButtonOnClick = () => {
//     let prevPageBtn = document.getElementById('prev-page-button');
//     prevPageBtn.onclick = () => {
//         pagination.page = pagination.page - 1;
//         loadSeats();
//     }
// }
//
// const changePageNavigation = () => {
//     let pageLabel = document.getElementById('page-number-label');
//     let nextPageBtn = document.getElementById('next-page-button');
//     let prevPageBtn = document.getElementById('prev-page-button');
//
//     pageLabel.innerText = pagination.page + 1;
//
//     if (pagination.totalPages - pagination.page === 1) {
//         nextPageBtn.disabled = true;
//     } else {
//         nextPageBtn.disabled = false;
//     }
//
//     if (pagination.page === 0) {
//         prevPageBtn.disabled = true;
//     } else {
//         prevPageBtn.disabled = false;
//     }
// }
//
// const sortingSelectOnChange = () => {
//     let sortingSelect = document.getElementById('sort-field-select');
//     sortingSelect.onchange = () => {
//         pagination.page = 0
//         loadSeats();
//     }
// }
//
// //  http://localhost:8080/items?shopId=7&pagination.page=1&pagination.size=5
// const loadSeats = () => {
//      // let sortingSelect = document.getElementById('sort-field-select');
//     let url = "http://localhost:8080/seats?movieId=" + movie.id
//         + '&pagination.page=' + pagination.page
//         + '&pagination.size=' + pagination.size;
//     // + sortingSelect.value;
//
//     let request = httpGet(url);
//     if (request.status === 200) {
//         let response = JSON.parse(request.response);
//         pagination.totalElements = response.totalElements;
//         pagination.totalPages = response.totalPages;
//         // console.log(response);
//         mapSeatsToTable(response.data);
//         changePageNavigation();
//     }
// }
//
// const mapSeatsToTable = (seats) => {
//     if (seats.length > 0) {
//         appendSeatsTable();
//         for (let index in seats) {
//             if (seats[index]) {
//                 insertSeatToTable(seats[index]);
//             }
//         }
//     }
// }
//
// const insertSeatToTable = (seat) => {
//     let table = document.getElementById('seats-table');
//     // let imageUrl = seat.image ? 'http://localhost:8080/image/' + seat.image : 'https://cdn3.vectorstock.com/i/1000x1000/21/87/blank-item-icon-template-with-red-big-sale-top-vector-32662187.jpg';
//     table.innerHTML = table.innerHTML +
//         `<tr>
//         <td class = "right-content id-text" >${seat.id} </td>
//        <!-- <td class="left-content left-inner-space"> ${seat.name}</td>
//         <td class="right-content">${seat.price}₴</td>
//         <td class="right-content">
//         <img src="${imageUrl}" alt="">
//         </td> -->
//         <td class="right-content">
//               <button class="seat-edit-btn edit-btn" seatid="${seat.id}">Edit</button>
//             <button class="seat-delete-btn dark-btn" seatid="${seat.id}">Delete</button>
//         </td>
//         </tr>`
// }
//
// const appendSeatsTable = () => {
//     let container = document.getElementById('seats-container');
//     container.innerHTML =
//         `<table id="seats-table">
// <th class="right-content">id</th>
// <!-- <th class="left-content left-inner-space ">name</th>
// <th class="right-content">price</th> -->
// </table>`
// }
//
// const openModal = () => {
//     document.getElementById('open-categories-modal').onclick = () => {
//         let modal = new Modal(
//             'categories',
//             getCategoriesModal()
//         );
//         modal.open();
//         let checkboxes = document.getElementsByClassName('movie-category-check');
//         for (let checkbox of checkboxes) {
//             if (categoryContainsById(checkbox.value)) {
//                 checkbox.checked = true;
//             }
//         }
//
//         let saveCategoriesButton = document.getElementById('save-categories-button');
//
//         saveCategoriesButton.onclick = () => {
//
//             const movieRequest = {
//                 name: movie.name,
//                 categoryIds: []
//             }
//             for (let checkbox of checkboxes) {
//                 if (checkbox.checked) {
//                     movieRequest.categoryIds.push(+checkbox.value);
//                 }
//             }
//             httpPut('http://localhost:8080/movies/' + movie.id, JSON.stringify(movieRequest));
//             modal.close();
//             loadMovie();
//         }
//     };
// }
//
// const categoryContainsById = (id) => {
//     for (let category of movie.categories) {
//         console.log(category.id + ' and found for ' + id);
//         if (category.id == id) {
//             return true;
//         }
//     }
//     return false;
// }
//
// const getCategoriesModal = () => {
//     let request = httpGet('http://localhost:8080/categories');
//     let response = JSON.parse(request.response);
//
//     let modal = `<div class="modal-content-container small-modal">
//                 <div class="modal-header">Categories</div>
//                 <div class="modal-content min-h120" id="categories-container">`;
//     for (let category of response) {
//         modal  = modal + getWrapperForCategoryInModal(category);
//     }
//     modal = modal +
//         `</div>
//                 <div class="modal-footer">
//                 <button id="save-categories-button" class="dark-btn modal-save-button">Save</button>
//                 </div>
//             </div>`
//     return modal;
// }
//
// const getWrapperForCategoryInModal = (category) => {
//     return `<div class="check-element flex-container vertical-center">
//                 <input class="check-input movie-category-check" id="${category.id + '-category'}" type="checkbox" value="${category.id}">
//                 <label class="checkbox-label" for="${category.id + '-category'}">${category.name}</label>
//             </div>`
// }
