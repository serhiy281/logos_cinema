// import {httpGet, httpDelete, httpPut, httpPost} from './http-tools.js'
// import {appendHeader} from './header.js'
//
// let movieList = {};
//
// document.addEventListener('DOMContentLoaded', function () {
//     appendHeader().then(r => {
//
//         setCreateMovieButtonOnClick();
//         loadMovies();
//     });
// });
//
// const setCreateMovieButtonOnClick = () => {
//     let button = document.getElementById('create-movie-button');
//     console.log(button);
//     console.log('set create click');
//     button.onclick = () => {
//         let movieIdInput = document.getElementById('movie-id-input');
//         let movieNameInput = document.getElementById('movie-name-input ');
//         let movieCreateButton = document.getElementById('create-movie-button');
//         console.log('click');
//         if (movieNameInput.value) {
//             if (!movieIdInput.value) {
//                 const movie = {
//                     name: movieNameInput.value
//                 }
//                 let request = httpPost("http://localhost:8080/movies", JSON.stringify(movie));
//                 if (request.status === 200) {
//                     movieNameInput.value = '';
//                     loadMovies();
//                 }
//             } else {
//                 const movie = {
//                     name: movieNameInput.value
//                 }
//                 let request = httpPut("http://localhost:8080/movies/" + movieIdInput.value, JSON.stringify(movie));
//                 if (request.status === 200) {
//                     movieNameInput.value = '';
//                     movieCreateButton.innerText = 'Create';
//                     loadMovies();
//                 }
//             }
//         }
//     }
// }
//
// const loadMovies = () => {
//     let request = httpGet("http://localhost:8080/movies");
//
//     let response = JSON.parse(request.response);
//     if (request.status === 200) {
//         mapMoviesFromResponse(response);
//     }
// }
//
// const mapMoviesFromResponse = (movies) => {
//     if (movies.length > 0) {
//         appendMoviesTable();
//         for (let index in movies) {
//             appendMovieToTable(movies[index]);
//             movieList[movies[index].id] = movies[index];
//         }
//         addOnClickActionEditButtons();
//         addOnClickActionDeleteButtons();
//     } else {
//         let container = document.getElementById('movies-table-container');
//         container.innerHTML = `<h1>Movie list is empty</h1>`
//     }
// }
//
// const mapMoviesFromList = () => {
//     if (movieList) {
//         appendMoviesTable();
//         for (let index in movieList) {
//             if (movieList[index]) {
//                 appendMovieToTable(movieList[index]);
//             }
//         }
//         addOnClickActionEditButtons();
//         addOnClickActionDeleteButtons();
//     } else {
//         let container = document.getElementById('movies-table-container');
//         container.innerHTML = `<h1>Movie list is empty</h1>`
//     }
// }
//
// const appendMoviesTable = () => {
//     let container = document.getElementById('movies-table-container');
//     container.innerHTML =
//         `<table id="movies-table">
//             <tr>
//                 <th class="right-content">id</th>
//                 <th class="left-content left-inner-space">name</th>
//                 <th></th>
//             </tr>
//          </table>`
// }
//
// const appendMovieToTable = (movie) => {
//     let table = document.getElementById('movies-table');
//     let url = 'http://localhost:8080/admin/movie-manage/' + movie.id;
//     table.innerHTML = table.innerHTML +
//         `<tr>
//         <td class="right-content id-text">${movie.id}</td>
//         <td class="left-content left-inner-space">${movie.name}</td>
//         <td>
//             <a target="_blank" class="details-link" href="${url}">Details</a>
//         </td>
//         <td class="right-content">
//             <button class="movie-edit-btn edit-btn" movieid="${movie.id}">Edit</button>
//             <button class="movie-delete-btn dark-btn" movieid="${movie.id}">Delete</button>
//         </td>
//         </tr>`
// }
//
// const addOnClickActionEditButtons = () => {
//     let buttons = document.getElementsByClassName('movie-edit-btn');
//     for (let i = 0; i < buttons.length; i++) {
//         let button = buttons[i];
//         let movieId = button.getAttribute('movieid');
//
//         button.onclick = () => {
//             let movieIdInput = document.getElementById('movie-id-input');
//             let movieNameInput = document.getElementById('movie-name-input');
//             let movieCreateButton = document.getElementById('create-movie-button');
//             movieIdInput.value = movieId;
//             movieNameInput.value = movieList[movieId].name;
//             movieCreateButton.innerText = 'Save';
//         }
//     }
// }
//
// const addOnClickActionDeleteButtons = () => {
//     let buttons = document.getElementsByClassName('movie-delete-btn');
//     for (let i = 0; i < buttons.length; i++) {
//         let button = buttons[i];
//         let movieId = button.getAttribute('movieid');
//         button.onclick = () => {
//             let request = httpDelete("http://localhost:8080/movies/" + movieId);
//             console.log(movieList);
//             if (request.status === 200) {
//                 delete movieList[movieId];
//                 console.log(movieList);
//                 mapMoviesFromList();
//             }
//         }
//     }
// }